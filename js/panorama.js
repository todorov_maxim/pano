window.THREE = THREE;

let scene;

let notNull = arg => arg != undefined || arg != null;

const config = {
    dps: 36,
    fov: 80,
    backgroundImage: './assets/pbg-min.jpg',
};


window.addEventListener('load', () => {
    new Panorama(document.getElementById('panorama-place'), config);
});


const infoList = [];

/*
 * Panorama
 */
const Panorama = (() => {
    const panoramaDiameter = 500;
    let uvList = [];
    const meshList = new THREE.Object3D();

    function onWheel({deltaY}) {
        this.zoom(deltaY * 0.05);
    }

    function onMouseDown(e) {
        const {button, clientX, clientY} = e;

        if (button !== 2)
            return;

        const {userInteracting} = this;
        const {lon, lat} = userInteracting;

        e.preventDefault();

        userInteracting.mouseDown = true;

        userInteracting.onMouseDownMouseX = clientX;
        userInteracting.onMouseDownMouseY = clientY;

        userInteracting.onMouseDownLon = lon;
        userInteracting.onMouseDownLat = lat;
    }

    function onMouseMove(e) {
        const {buttons, clientX, clientY} = e;

        if (buttons === 2) {
            const {userInteracting} = this;
            const {onMouseDownMouseX, onMouseDownMouseY, onMouseDownLon, onMouseDownLat, mouseDown} = userInteracting;

            if (mouseDown) {
                userInteracting.lon = (onMouseDownMouseX - clientX) * 0.075 + onMouseDownLon;
                userInteracting.lat = (clientY - onMouseDownMouseY) * 0.075 + onMouseDownLat;
            }
        } else if (buttons === 0) {
            const {layerX, layerY} = e;
            const {raycaster, sphere, camera} = this;
            const {mouse, point} = raycaster;
            const {clientWidth, clientHeight} = this.renderer.domElement;

            mouse.x = (layerX / clientWidth) * 2 - 1;
            mouse.y = -(layerY / clientHeight) * 2 + 1;

            raycaster.setFromCamera(mouse, camera);

            const intersect = raycaster.intersectObject(sphere)[0];


            if (!notNull(intersect))
                return;


            meshList.children.forEach(obj3d => obj3d.visible = false);
            const intersectHoverList = raycaster.intersectObjects(meshList.children);
            intersectHoverList.forEach(intersectHoverItem => {
                if (intersectHoverItem) {
                    intersectHoverItem.object.visible = getPixelColor(intersectHoverItem.uv, intersectHoverItem.object);
                }
            });


            Object.assign(point, intersect.point);


            if (meshList.children.length) {
                meshList.children.forEach(convexMesh => convexMesh.material.opacity = 0.5);
                const intersectConvex = raycaster.intersectObjects(meshList.children);
                if (intersectConvex.length) {
                    intersectConvex.forEach(intersectConvexData => intersectConvexData.object.material.opacity = 0.8);
                }

            }
        } else {
            return;
        }

        e.preventDefault();
    }

    function onMouseUp(e) {
        this.userInteracting.mouseDown = false;
    }

    let line;
    let points;

    function enter(e) {
        e.preventDefault();

        if (line) {
            if (points.length < 3) { // todo
                line.parent.remove(line);
            } else {
                addPointsBetweenTwoPoints(points[points.length - 1], points[0]);

                points.push(points[0]);

                line.geometry = new THREE.BufferGeometry().setFromPoints(points);

                meshList.add(getDrawMesh(uvList));
            }
            line = null;
        }
    }

    Keybinder.bind('Enter', enter);
    Keybinder.bind('NumpadEnter', enter);

    function onClick(e) {
        let {raycaster, polygonScene, scene} = this;
        let {point} = raycaster;

        if (!line) {
            line = new THREE.Line(new THREE.BufferGeometry(), new THREE.LineBasicMaterial({color: 0xff0000}));
            points = [];
            uvList = [];
            // polygonScene.add(line);
            scene.add(line);
        }

        const mouse = new THREE.Vector2();
        mouse.x = (e.layerX / this.renderer.domElement.clientWidth) * 2 - 1;
        mouse.y = -(e.layerY / this.renderer.domElement.clientHeight) * 2 + 1;

        raycaster.setFromCamera(mouse, this.camera);

        const intersect = raycaster.intersectObject(this.sphere)[0];

        if (points.length > 0) {

            addPointsBetweenTwoPoints(points[points.length - 1], point);

        }

        uvList.push(intersect.uv);
        points.push(point.clone());

        line.geometry = new THREE.BufferGeometry().setFromPoints(points);
    }

    function Panorama(domParentElement, config) {
        this.config = Object.assign({
            fov: 50,
            dps: 36,
        }, config);

        let {fov, backgroundImage} = this.config;

        // renderer
        this.renderer = new THREE.WebGLRenderer({preserveDrawingBuffer: true, antialias: true});
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(domParentElement.clientWidth, domParentElement.clientHeight);
        this.renderer.autoClear = false;

        domParentElement.appendChild(this.renderer.domElement);

        // scene
        scene = this.scene = new THREE.Scene();
        this.scene.add(meshList);
        this.polygonScene = new THREE.Scene();
        this.spriteScene = new THREE.Scene();

        // camera
        this.camera = new THREE.PerspectiveCamera(fov, domParentElement.clientWidth / domParentElement.clientHeight, .1, 3000);
        this.camera.target = new THREE.Vector3(0, 0, 0);

        // sphere
        this.sphere = new THREE.Mesh(
            new THREE.SphereBufferGeometry(panoramaDiameter, 60, 40),
            new THREE.MeshBasicMaterial({
                side: THREE.BackSide,
                wireframe: true,
            })
        );
        this.sphere.rotation.y = Math.PI;

        if (notNull(backgroundImage)) {
            let material = this.sphere.material;
            material.wireframe = false;
            let texture = material.map = new THREE.TextureLoader().load(backgroundImage, () => {
                document.getElementById('preloader').classList.add('hide');
            });
            texture.anisotropy = this.renderer.getMaxAnisotropy();
            texture.minFilter = THREE.LinearFilter;
            material.needsUpdate = true;
        }

        this.sphere.scale.x = -1;

        this.scene.add(this.sphere);

        // raycaster
        this.raycaster = new THREE.Raycaster();
        this.raycaster.mouse = new THREE.Vector2();
        this.raycaster.point = new THREE.Vector3();

        this.userInteracting = {
            mouseDown: false,
            onMouseDownMouseX: 0,
            onMouseDownMouseY: 0,
            lon: 0,
            onMouseDownLon: 0,
            lat: 0,
            onMouseDownLat: 0,
        };

        domParentElement.addEventListener('mousedown', onMouseDown.bind(this), false);
        document.addEventListener('mousemove', onMouseMove.bind(this), false);
        document.addEventListener('mouseup', onMouseUp.bind(this), false);

        domParentElement.addEventListener('contextmenu', e => e.preventDefault(), false);

        domParentElement.addEventListener('wheel', onWheel.bind(this), false);

        domParentElement.addEventListener('click', onClick.bind(this), false);

        /* Animate */
        let lastTs = 0;
        let animate = (ts = 0) => {
            requestAnimationFrame(animate);
            this.update(ts - lastTs);
            lastTs = ts;
        };

        window.addPointsBetweenTwoPoints = (prevP, point, steps) => {
            let {raycaster} = this;
            const betweenSteps = steps || 50;
            const dp = new Vector3().subVectors(point, prevP).multiplyScalar(1 / betweenSteps);
            const cp = prevP.clone();

            for (let i = 0; i < betweenSteps - 1; i++) {
                cp.add(dp);

                raycaster.set(this.camera.position, cp);
                const itsc = raycaster.intersectObject(this.sphere)[0];

                uvList.push(itsc.uv);
                // points.push(cp.clone());
            }
        };

        animate();
    }

    Panorama.prototype.zoom = function (n) {
        let {camera, config} = this;
        let {fov: maxFov} = config;

        let fov = camera.fov + n;
        camera.fov = THREE.MathUtils.clamp(fov, 10, maxFov);
        camera.updateProjectionMatrix();
    };

    Panorama.prototype.update = function (ms) {
        let {config, renderer, scene, spriteScene, polygonScene, camera, userInteracting} = this;
        let {lon, lat} = userInteracting;

        // keyboard rotation
        let {dps} = config;
        let {ArrowLeft, ArrowRight, ArrowUp, ArrowDown} = Keybinder.keys;

        dps = ms / 1000 * dps;

        if (ArrowLeft ^ ArrowRight)
            lon += dps * (ArrowLeft ? -1 : 1);
        if (ArrowUp ^ ArrowDown)
            lat += dps * (ArrowUp ? 1 : -1);

        // keyboard zoom
        let {Equal, Minus, NumpadAdd, NumpadSubtract} = Keybinder.keys;

        let increase = Equal || NumpadAdd;
        let reduce = Minus || NumpadSubtract;

        if (increase ^ reduce)
            this.zoom(ms / 20 * (increase ? -1 : 1));

        // angle limit
        let {angleLimit} = config;

        if (notNull(angleLimit)) {
            angleLimit /= 2;
            if (lon > angleLimit)
                lon = angleLimit;
            else if (lon < -angleLimit)
                lon = -angleLimit;
        }

        if (lat > 90)
            lat = 90;
        else if (lat < -90)
            lat = -90;

        if (lon > 180)
            lon -= 360;
        else if (lon < -180)
            lon += 360;

        userInteracting.lon = lon;
        userInteracting.lat = lat;


        // render
        lat = Math.max(-85, Math.min(85, lat));
        let phi = THREE.MathUtils.degToRad(90 - lat);
        let theta = THREE.MathUtils.degToRad(lon);

        camera.target.x = panoramaDiameter * Math.sin(phi) * Math.cos(theta);
        camera.target.y = panoramaDiameter * Math.cos(phi);
        camera.target.z = panoramaDiameter * Math.sin(phi) * Math.sin(theta);

        camera.lookAt(camera.target);

        renderer.clear();
        renderer.render(scene, camera);
        // renderer.clearDepth();
        // renderer.render(polygonScene, camera);
        // renderer.clearDepth();
        // renderer.render(spriteScene, camera);
    };

    return Panorama;
})();

function getDrawMesh(uvList) {

	let swap = false;

	// check
	let left = false;
	let right = false;
	let center = false;

	let upper = true;

	uvList.forEach((uv) => {
		if (uv.x > 0.9) {
			right = true;
		} else if (uv.x < 0.1) {
			left = true;
		} else if (uv.x > 0.45 && uv.x < 0.55) {
			center = true;
			if (uv.y < 0.5) {
				upper = false;
			}
		}
	});

	const side = left && right && center;


	// uv update
	for (let ln = uvList.length, i = ln - 1; i >= 0; i --) {
		const uvp = (i === ln - 1) ? uvList[0] : uvList[i + 1];
		const uv = uvList[i];

		if (Math.abs(uvp.x - uv.x) > 0.5 && !side) {
			swap = true;

			if(uvp.x < 0.5) {
				const yd = uv.y - uvp.y;
				const xd = uvp.x + (1 - uv.x);
				const per = uvp.x / xd;
				const intersectY = uvp.y + yd * per;

				const uv1 = new THREE.Vector2(0.00000001, intersectY);
				const uv2 = new THREE.Vector2(0.99999999, intersectY);

				uvList.splice(i + 1, 0, uv1);
				uvList.splice(i + 1, 0, uv2);
			} else {
				const yd = uvp.y - uv.y;
				const xd = uv.x + (1 - uvp.x);
				const per = uv.x / xd;
				const intersectY = uv.y + yd * per;

				const uv1 = new THREE.Vector2(0.99999999, intersectY);
				const uv2 = new THREE.Vector2(0.00000001, intersectY);

				uvList.splice(i + 1, 0, uv1);
				uvList.splice(i + 1, 0, uv2);
			}
		}
	}

	let figure1 = [];
	let figure2 = [];

	if (swap) {
		uvList.forEach((item) => {
			if (item.x < 0.5) {
				figure1.push(item);
			} else {
				figure2.push(item);
			}
		});
	} else {
		figure1 = [...uvList];
	}

	//
	const drawCanvas = document.createElement('canvas');
	const ctx = drawCanvas.getContext('2d');
	drawCanvas.width = 1024 * 2;
	drawCanvas.height = 512 * 2;

	ctx.lineWidth = 2;

	const path1 = new Path2D();

	const mr = new THREE.Vector2(); // most right
	const ml = new THREE.Vector2(1, 1); // most left
	const pp = new THREE.Vector2(); // previous point

	if (!side) {
		figure1.forEach((uv, index) => {
			if (mr.x < uv.x) {mr.set(uv.x, (1 - uv.y))}
			if (ml.x > uv.x) {ml.set(uv.x, (1 - uv.y))}
	
			if (index === 0) {
				path1.moveTo(uv.x * drawCanvas.width, (1 - uv.y) * drawCanvas.height);
				pp.set(uv.x, uv.y);
				return;
			}
	
			if ( Math.abs(pp.x - uv.x) > 0.5 ) {
				path1.moveTo(uv.x * drawCanvas.width, (1 - uv.y) * drawCanvas.height);
			} else {
				path1.lineTo(uv.x * drawCanvas.width, (1 - uv.y) * drawCanvas.height);
			}
	
			pp.set(uv.x, uv.y);
		});
	} else {
		const yp = upper ? 0 : 1;

		// figure1.sort((a, b) => a.x - b.x);

		// path1.moveTo(0, yp * drawCanvas.height);

		// path1.lineTo(0, (1 - figure1[0].y) * drawCanvas.height);

		let prev = figure1[0].clone();
		// let leftPoint = new Vector2();
		// let rightPoint = new Vector2();

		figure1.forEach((uv) => {
			if ( Math.abs(prev.x - uv.x) > 0.5 ) {

				if (prev.x < 0.5) {

					path1.lineTo(0, (1 - prev.y) * drawCanvas.height);
					path1.lineTo(0, yp * drawCanvas.height);
					path1.lineTo(1 * drawCanvas.width, yp * drawCanvas.height);
					path1.lineTo(1 * drawCanvas.width, (1 - uv.y) * drawCanvas.height);

				} else {

					path1.lineTo(1 * drawCanvas.width, (1 - prev.y) * drawCanvas.height);
					path1.lineTo(1 * drawCanvas.width, yp * drawCanvas.height);
					path1.lineTo(0, yp * drawCanvas.height);
					path1.lineTo(0, (1 - uv.y) * drawCanvas.height);

				}
			}

			path1.lineTo(uv.x * drawCanvas.width, (1 - uv.y) * drawCanvas.height);

			prev.copy(uv);
		});

		// path1.lineTo(1 * drawCanvas.width, (1 - figure1[figure1.length - 1].y) * drawCanvas.height);

		// path1.lineTo(1 * drawCanvas.width, yp * drawCanvas.height);
	
		// path1.closePath();
	}
	
	path1.closePath();

	ctx.fill(path1);
	// ctx.stroke(path1);

	if (swap) {
		const path2 = new Path2D();

		figure2.forEach((uv, index) => {
			if (index === 0) {
				path2.moveTo(uv.x * drawCanvas.width, (1 - uv.y) * drawCanvas.height);
				return;
			}

			path2.lineTo(uv.x * drawCanvas.width, (1 - uv.y) * drawCanvas.height);
		});
		// path2.closePath();

		ctx.fill(path2);
		// ctx.stroke(path2);
	}

	// sphere
	const sphereHover = new THREE.Mesh(
		new THREE.SphereBufferGeometry(500 - 1, 32, 32),
		// new THREE.BoxBufferGeometry(100, 100, 100),
		new THREE.MeshBasicMaterial({
			side: THREE.BackSide,
			map: new THREE.CanvasTexture(drawCanvas),
			transparent: true,
			opacity: 0.5,
		})
	);
	sphereHover.scale.x = -1;
	sphereHover.rotation.y = Math.PI;
	sphereHover.visible = false;
	sphereHover.userData.index = infoList.length;
	sphereHover.userData.canvas = drawCanvas;
	sphereHover.userData.ctx = ctx;



	return sphereHover;
}

function getPixelColor(uvCoordinates, object3d) {
	const pixelData = object3d.userData.ctx.getImageData(uvCoordinates.x * object3d.userData.canvas.width, (1 - uvCoordinates.y) * object3d.userData.canvas.height, 1, 1).data;
	return !!pixelData[3];
}
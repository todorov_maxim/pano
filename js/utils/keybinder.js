var Keybinder = (() => {
    var keys = {};
    var binds = [];
    var funcKeys = [
        ['altKey', 'Alt'],
        ['ctrlKey', 'Ctrl'],
        ['metaKey', 'Meta'],
        ['shiftKey', 'Shift'],
    ];

    function bind(keyset, callback) {
        if (typeof keyset === 'string')
            keyset = keyset.split('+');

        for (let i = 0; i < keyset.length; i++)
            keyset[i] = keyset[i].replace(/\s/g, '').replace(/Win|Cmd/, 'Meta');

        var cleanKeyset = [];
        keyset.forEach(key => cleanKeyset.indexOf(key) === -1 && cleanKeyset.push(key));

        binds.push({keyset: cleanKeyset, callback});
    }

    window.addEventListener('keydown', e => {
        keys[e.code] = true;
        funcKeys.forEach(key => keys[key[1]] = e[key[0]]);

        binds.forEach(bind => {
            var {keyset, callback} = bind;

            for (var i = 0; i < keyset.length; i++)
                if (!keys[keyset[i]])
                    break;

            if (i === keyset.length)
                callback(e);
        });
    });

    window.addEventListener('keyup', e => {
        keys[e.code] = false;
        funcKeys.forEach(key => keys[key[1]] = e[key[0]]);
    });

    window.addEventListener('blur', () => {
        for (var key in keys)
            keys[key] = false;

        funcKeys.forEach(key => keys[key[1]] = false);
    });

    return {keys, binds, bind};
})();
